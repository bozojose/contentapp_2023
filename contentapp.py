#!/usr/bin/python3

import webapp


class ContentApp(webapp.webApp):

    #  diccionario con el contenido para los recursos disponibles
    contenido = {'/': 'Inicio',
                 '/frio': 'Hace frío',
                 '/calor': 'Hace calor',
                 '/lluvia': 'Está lloviendo',
                 '/sol': 'El sol brilla intensamente'
                 }

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        _, recurso, _ = request.split(' ', 2)  # Trocea los dos primeros
        # espacios en blanco y guarda la segunda parte, el resto lo deshecha

        return recurso

    def process(self, recurso):
        """Process the relevant elements of the request."""

        print('El recurso es: ' + recurso)

        if recurso in self.contenido.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.contenido[recurso] + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "<html><body>El recurso " + recurso + " no forma parte de los contenidos.</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = ContentApp("localhost", 1234)
